interface ObjectWithCountAndValue {
    count: number;
    value: number;
    average: number;
}

type RecursiveObjectWithCountAndValue =
    | ObjectWithCountAndValue
    | Record<string, ObjectWithCountAndValue>;

type RecurisiveMap = {[key: string]: number | RecurisiveMap};

const recursivelyAverageObjects = (
    result: RecursiveObjectWithCountAndValue,
    object: RecurisiveMap
) => {
    Object.entries(object).forEach(([key, val]) => {
        const res = result as Record<string, ObjectWithCountAndValue>;

        if (typeof val === "object") {
            if (typeof res[key] === "undefined") {
                // @ts-ignore
                res[key] = {};
            }
            recursivelyAverageObjects(res[key], val);
            return;
        }

        if (typeof res[key] === "undefined") {
            res[key] = {count: 1, value: val, average: val};
        } else {
            let {count, value, average} = res[key];
            count++;
            value += val;
            average = value / count;
            res[key] = {count, value, average};
        }
    });

    return result;
};

const extractAverage = (
    obj: RecursiveObjectWithCountAndValue
): number | RecurisiveMap => {
    if (typeof obj.average === "undefined") {
        return recursivelyExtractAverage(obj);
    }

    return obj.average as number;
};

const recursivelyExtractAverage = (
  object: RecursiveObjectWithCountAndValue
): RecurisiveMap =>
  Object.entries(object).reduce((acc, [k, v]) => {
      acc[k] = extractAverage(v);
      return acc;
  }, {} as RecurisiveMap);


export const getAverageOfObjects = (objects: RecurisiveMap[]) => {
    const objectWithCountAndValue = objects.reduce(
        recursivelyAverageObjects,
        {} as RecursiveObjectWithCountAndValue
    );

    return recursivelyExtractAverage(objectWithCountAndValue);
};

export const recursivelyExecutePromises = async (
    promises: (() => Promise<any>)[]
) => {
    // TODO: Complete the getAverageOfObjects function

    return;
};
