export class Stack<T> {
    private array: T[];

    constructor() {
        this.array = [];
    }

    push(element: T): void {
        this.array.push(element);
    }

    pop(): T {
        return this.array.pop();
    }

    peek(): T {
        return this.array[this.array.length - 1];
    }

    public get length() : number {
        return this.array.length;
    }

    toArray(): T[] {
        return this.array;
    }
}
