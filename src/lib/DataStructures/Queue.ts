export class Queue<T> {
    private array: T[];

    constructor() {
        this.array = [];
    }

    public get length() : number {
        return this.array.length;
    }

    enqueue(element: T): void {
        this.array = [element, ...this.array];
    }

    dequeue(): T {
        return this.array.pop();
    }

    toArray(): T[] {
        return this.array.reverse();
    }
}
