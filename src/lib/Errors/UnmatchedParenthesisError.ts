export class UnmatchedParenthesisError extends Error {
    constructor(message?: string) {
        super(message);
    }
}
