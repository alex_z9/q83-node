export const statsObjects = [
  {
    hits: 100,
    misses: 30,
    chillies: {
      jalapeno: 2,
      birdsEye: 4
    }
  },
  {
    hits: 20,
    misses: 10,
    jumps: 5,
    chillies: {
      jalapeno: 8,
      birdsEye: 7
    }
  }
]