import {Stack, Queue} from "./DataStructures";
import {UnmatchedParenthesisError} from "./Errors/UnmatchedParenthesisError";

type Operand = "+" | "-" | "*" | "/";
type OperandWithParenthesis = Operand | "(" | ")";
const Operands = ["+", "-", "*", "/"];

const OperatorPriority: Record<Operand, number> = {
    "/": 1,
    "*": 1,
    "+": 0,
    "-": 0,
};

const doesCountOfOpeningBracetsMatchCountOfClosingBrackets = (
    array: string[]
): boolean => {
    const {"(": opening, ")": closing} = array.reduce(
        (acc, operator) => {
            if (operator === ")" || operator === "(") {
                acc[operator] += 1;
            }

            return acc;
        },
        {"(": 0, ")": 0}
    );

    return opening === closing;
};

const stripWhitespaceFromStringAndSplitOnOperands = (str: string): string[] =>
    str
        .replace(/\s+/g, "")
        .split(/([\+\-\*\/\(\)])/)
        .filter((s) => s !== "");

function isNumber(input: any): input is number {
    return !isNaN(parseFloat(input)) && isFinite(input);
}

function isOperand(input: any): input is Operand {
    return Operands.includes(input);
}

/**
 * Takes an infix notation expression and returns the reverse polish notation
 * @param infixExpression - infix expression - e.g "4 * 2 + (44-2) * 2"
 */
const getReversePolishNotationFromInfix = (infixExpression: string): string => {
    const outputQueue = new Queue<string>();
    const operatorStack = new Stack<OperandWithParenthesis>();
    const infixArray = stripWhitespaceFromStringAndSplitOnOperands(
        infixExpression
    );

    infixArray.forEach((token) => {
        if (isNumber(token)) {
            outputQueue.enqueue(token);
        } else if (isOperand(token)) {
            const o1 = token;
            let o2 = operatorStack.peek();

            while (
                isOperand(o2) &&
                OperatorPriority[o1] <= OperatorPriority[o2]
            ) {
                outputQueue.enqueue(operatorStack.pop());
                o2 = operatorStack.peek();
            }
            operatorStack.push(o1);
        } else if (token === "(") {
            operatorStack.push(token);
        } else if (token === ")") {
            while (operatorStack.peek() !== "(") {
                outputQueue.enqueue(operatorStack.pop());
                if (operatorStack.length === 0) {
                    // if we get to the bottom of the stack, then there is no matching opening bracket
                    throw new UnmatchedParenthesisError();
                }
            }
            operatorStack.pop();
        }
    });

    if (
        !doesCountOfOpeningBracetsMatchCountOfClosingBrackets(
            operatorStack.toArray()
        )
    ) {
        // if the number of opening brackets don't match the number of closing brackets that's an error
        throw new UnmatchedParenthesisError();
    }

    while (operatorStack.length > 0) {
        outputQueue.enqueue(operatorStack.pop());
    }

    return outputQueue.toArray().join(" ");
};

/**
 * Takes in a reverse polish notation expression, evaluates it and returns the result
 * @param expression - reverse polish notation expression - e.g "3 4 +"
 */
const evaluateReversePolishNotationExpression = (
    expression: string
): number => {
    const resultStack = new Stack<number>();
    const expressionArray = expression.split(" ");

    expressionArray.forEach((token) => {
        if (isNumber(token)) {
            resultStack.push(Number(token));
        } else {
            const a = resultStack.pop();
            const b = resultStack.pop();

            if (token === "+") {
                resultStack.push(a + b);
            } else if (token === "-") {
                resultStack.push(b - a);
            } else if (token === "*") {
                resultStack.push(a * b);
            } else if (token === "/") {
                resultStack.push(b / a);
            }
        }
    });

    if (resultStack.length > 1) {
        throw Error();
    } else {
        return resultStack.pop();
    }
};

export {
    getReversePolishNotationFromInfix,
    evaluateReversePolishNotationExpression,
};
