import express from "express";
import {
    evaluateReversePolishNotationExpression,
    getReversePolishNotationFromInfix,
} from "./lib/equations";
import {UnmatchedParenthesisError} from "./lib/Errors";

const app = express();
const port = 3000;

app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.get("/equation/:equation", (req, res) => {
    const {equation} = req.params as {equation: string};

    if (/[^0-9\-*+\/\(\)]/.test(equation)) {
        return res.status(400).json({
            message: `The equation you entered contains invalid characters. Valid characters are digits (0-9) or the operators "+", "-", "/", "*", "(", ")".
                 If you are trying to do division, please use the URL Encoded forward slash "%2F".`,
        });
    }

    try {
        const polishNotation = getReversePolishNotationFromInfix(equation);
        const result = evaluateReversePolishNotationExpression(polishNotation);

        return res.json({equation, result});
    } catch (e) {
        if (e instanceof UnmatchedParenthesisError) {
            return res.status(400).json({
                message: "Your expression contained an unmatched parenthesis",
            });
        }

        res.status(500).json({message: "Something went wrong"});
    }
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
